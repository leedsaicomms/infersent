import torch
from pathlib import Path
from infersent.models import InferSent
from os.path import dirname


def load_model(model_params):
    """
    Loads the selected InferSent model based on the version.
    :param model_params: the model parameters
    :return: the InferSent model
    :exception: ValueError if model_version is <1 or >2.
    """
    model_version = model_params.get('version', 0)

    if not(model_version == 1 or model_version == 2):
        raise ValueError("Model Version must be either 1 or 2!")

    base = (Path(dirname(__file__)) / 'encoder').resolve()
    model_path = base / 'infersent1.pkl' if model_version == 1 else base / 'infersent2.pkl'

    model = InferSent(model_params).cuda() if torch.cuda.device_count() > 0 else InferSent(model_params)
    model.load_state_dict(torch.load(model_path))

    return model
