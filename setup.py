import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="infersent",
    version="1.0.0",
    author="Facebook Research",
    description="Sentence embeddings (InferSent) and training code for NLI.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/facebookresearch/InferSent",
    packages=['infersent'],
    package_data={'infersent': ['encoder/*.pkl']},
    include_package_data=True,
    install_requires=['torch', 'torchvision', 'nltk'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
